import React, { Component } from "react";

class App extends Component {
  hadnleLink = () => {
    this.props.history.push("/suscripcion");
  };

  render() {
    return (
      <div className="App">
        <div className="slide-bg">
          <ul className="nav justify-content-end">
            <li className="nav-item">
              <a className="nav-link active" href="#">
                Equipo
              </a>
            </li>
            <li className="nav-item">
              <a class="nav-link" href="#">
                Blog
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#" to="/suscripcion">
                Contacto
              </a>
            </li>
          </ul>
          <img
            className="logo"
            src="https://i.ibb.co/XJnPL4x/latam-logo.png"
            alt=""
          />

          <div className="header-content">
            <h2 className="header-text">
              comienza una nueva vida y <br /> unete al mundo <br />
              tecnologico
            </h2>
            <button className="btn btn-primary " onClick={this.hadnleLink}>
              {" "}
              Comienza aca!
            </button>
            <button className="btn btn-success "> Mas info</button>
            <h5 className="text-h5">
              {" "}
              hay una escasez gigante de desarrolladores en el mundo y mas en
              latinoamerica, se neceistan mas <br /> desarrolladores y tu puedes
              ser uno
            </h5>
          </div>
        </div>
        <div className="content">
          <div className="col-sm-8 mx-auto">
            <h1>El boopcamp mas grande latinoamerica</h1>
            <p>
              orem ipsum dolor sit amet consectetur, adipiscing elit libero.
              Cras ultrices interdum erat sed praesent id, accumsan orci
              hendrerit at mollis pellentesque, malesuada tempus inceptos
              imperdiet suscipit. Fusce metus augue interdum platea placerat
              primis ullamcorper
            </p>
            <img
              src="https://i.ibb.co/WP0Fmd8/people.png"
              className="content-img"
              alt="content-img"
            />
          </div>
        </div>

        <main className="main" role="main">
          <div className="jumbotron">
            <div className="col-sm-8 mx-auto">
              <h1>Solicita mas informacion e inscribite</h1>
              <p>
                Número de unidades Texto HTML Copiar Lorem ipsum dolor sit amet
                consectetur, adipiscing elit libero. Cras ultrices interdum erat
                sed praesent id, accumsan orci hendrerit at mollis pellentesque,
                malesuada tempus inceptos imperdiet suscipit. Fusce metus augue
                interdum platea placerat primis ullamcorper fringilla
              </p>
              <button className="btn btn-primary" onClick={this.hadnleLink}>
                Inscribite
              </button>
              <p>Vive la experiencia latam</p>
            </div>
          </div>
        </main>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <p style={{ textAlign: "center" }}>
                Contacto
                <br />
                <strong>Desafio latam</strong>
                <br />
                Ezequias Allende 2361
                <br />
                P: 92983847528
              </p>
            </div>
          </div>
        </div>
        <footer class="footer mt-auto py-3">
          <div class="container">
            <span class="text-muted">Place sticky footer content here.</span>
          </div>
        </footer>
      </div>
    );
  }
}

export default App;
