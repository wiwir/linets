import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import App from "./App";
import Form from "./Form";

class Home extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route exact path="/" component={App} />
          <Route path="/suscripcion" component={Form} />
        </Switch>
      </div>
    );
  }
}

export default Home;
