import React from "react";
import axios from "axios";
import { withFormik, Field, ErrorMessage, Form } from "formik";
import "./style.scss";

function MyForm(props) {
  const { isSubmitting, isValid } = props;

  return (
    <React.Fragment>
      <div className="card mb-4 shadow-sm">
        <div className="card-header">
          <h4 className="my-0 font-weight-normal">Suscribete</h4>
          <Form>
            <div className="row-form">
              Email:
              <Field name="email" type="email" className="input" />
              <ErrorMessage name="email">
                {message => <div className="error">{message}</div>}
              </ErrorMessage>
            </div>
            <div className="row-form">
              Contraseña:
              <Field name="password" type="password" className="input-pass" />
              <ErrorMessage name="password">
                {message => <div className="error">{message}</div>}
              </ErrorMessage>
            </div>

            <div className="row-form">
              <button
                type="submit"
                className={`submit ${
                  isSubmitting || !isValid ? "disabled" : ""
                }`}
                disabled={isSubmitting || !isValid}
              >
                Inscribirme
              </button>
            </div>
          </Form>
        </div>
      </div>
    </React.Fragment>
  );
}

export default withFormik({
  mapPropsToValues(props) {
    return {
      email: "",
      password: ""
    };
  },

  validate(values) {
    const errors = {};

    if (!values.password) {
      errors.password = "Password is required";
    } else if (values.password.length < 8) {
      errors.password = "Password must be at least 8 characters";
    }
    return errors;
  },

  async handleSubmit(values, formikBag) {
    formikBag.setSubmitting(false);
    try {
      const response = await axios.post(
        `http://fake_api_newsletter.docker.ewok.cl:5005/api/newsletter/list/members?email=${values}`
      );
      console.log("👉 Returned data:", response);
    } catch (e) {
      console.log(`😱 Axios request failed: ${e}`);
    }
  }
})(MyForm);
